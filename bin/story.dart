import 'game.dart';

class story {
  void printIntro() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You are a Kid");
    print("You left alone in southern part of asia");
    print("Only thing you have to do is to Survive");
    g.anythingToContinue();
  }

  void story1() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You are an Adventure");
    print("After you survive all the conflict");
    print("You getting stronger and ready for the future");
    g.anythingToContinue();
  }

  void story2() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You are a Hunter");
    print("You hunt not for the money, but for your own life");
    print("No matter how strong you are,You are still on your own");
    g.anythingToContinue();
  }

  void story3() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You are a Warrior");
    print("You are the one man against the world");
    print(
        "No way to take you down alone. Might need the Strength of a Thousand Men");
    g.anythingToContinue();
  }

  void story4() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You are a Serial Killer");
    print(
        "You don't know anymore.What is the purpose?.Why you have to kill them?.");
    print("No one can stop you now...");
    g.anythingToContinue();
  }

  void storyEnd() {
    game g = game();
    g.clearConsole();
    g.printSeperator(30);
    print("Story");
    g.printSeperator(30);
    print("You finished the game");
    print("There is nothing more you can do now....");
    print("--------Thank you for playing--------");
    g.anythingToContinue();
  }
}
