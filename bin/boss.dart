import 'character.dart';
import 'dart:math';

class boss extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  boss(String name, int playerXp) : super(name, 1000, 100, 10) {
    this.playerXp = playerXp;
    this.skill = "Power Bomb";
  }

  @override
  int attack() {
    return 5 + Random().nextInt(5);
  }

  int uniAttack() {
    return 200;
  }

  @override
  int defend() {
    return 1;
  }
}
