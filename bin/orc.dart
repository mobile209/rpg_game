import 'character.dart';
import 'dart:math';

class orc extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  orc(String name, int playerXp) : super(name, 150, 50, 50) {
    this.playerXp = playerXp;
    this.skill = "Regenerate";
  }

  @override
  int attack() {
    return 25 + Random().nextInt(5);
  }

  int uniAttack() {
    return (xp + hp ~/ 10);
  }

  @override
  int defend() {
    return 10;
  }
}
