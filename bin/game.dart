import 'dart:io';
import 'boss.dart';
import 'centaur.dart';
import 'enemy.dart';
import 'goblin.dart';
import 'golem.dart';
import 'orc.dart';
import 'player.dart';
import 'slime.dart';
import 'story.dart';
import 'dart:math';

class game {
  late Player player;
  late bool isRunning;
  final random = new Random();
  List<String> encounters = ["Battle", "Battle", "Battle", "Shop"];
  late story s = story();
  int place = 0,
      act = 1,
      exp = 100,
      skill1 = 0,
      skill2 = 0,
      skill3 = 0,
      skill4 = 0;
  List<String> places = ["Satun", "Na Khon", "Phang nga", "Phuket", "Hat Yai"];
  int readInt(String promt, int userChoices) {
    int input;

    do {
      print(promt);
      try {
        input = int.parse(stdin.readLineSync()!);
      } on Exception {
        input = -1;
        print("Please enter an integer!");
      }
    } while (input < 1 || input > userChoices);
    return input;
  }

  void clearConsole() {
    for (int i = 0; i < 100; i++) {
      print("");
    }
  }

  void printSeperator(int n) {
    for (int i = 0; i < n; i++) {
      stdout.write("-");
    }
    print("");
  }

  void printHeading(String title) {
    printSeperator(30);
    print(title);
    printSeperator(30);
  }

  void anythingToContinue() {
    print("\nEnter anything to continue...");
    stdin.readLineSync()!;
  }

  void lvlUp() {
    if (player.xp >= exp) {
      player.xp -= exp;
      player.lvl++;
      exp += 50;
      player.maxHp += ((player.maxHp + 100));
      player.maxMp += ((player.maxMp + 100));
      print("You have Level up to lvl ${player.lvl}");
    }
  }

  void gameStart() {
    bool nameSet = false;
    String name;
    clearConsole();
    printSeperator(40);
    printSeperator(30);
    print("\x1B[31mTHE RUNAWAY BOY IN SOUTHERN SIDE\x1B[0m");
    print("\x1B[33mVERSION 0.0.2 alpha pre release\x1B[0m");
    printSeperator(30);
    printSeperator(40);
    anythingToContinue();

    do {
      clearConsole();
      printHeading("What's your name?");
      name = stdin.readLineSync()!;
      clearConsole();
      printHeading("Your name is $name.\nIs that correct?");
      print("(1) Yes!");
      print("(2) No, I want to change my name.");
      int input = readInt("->", 2);
      if (input == 1) nameSet = true;
    } while (!nameSet);

    player = Player(name);

    isRunning = true;
    s.printIntro();

    gameLoop();
  }

  void checkAct() {
    /// Start with Sa tun
    if (player.lvl >= 2 && act == 1) {
      ///Na khon
      act = 2;
      place = 1;
      s.story1();
      player.chooseTrait();
      player.hp = player.maxHp;
      player.mp = player.maxMp;
    } else if (player.lvl >= 3 && act == 2) {
      ///Phang nga
      act = 3;
      place = 2;
      s.story2();
      player.chooseTrait();
      player.hp = player.maxHp;
      player.mp = player.maxMp;
    } else if (player.lvl >= 4 && act == 3) {
      ///Phuket
      act = 4;
      place = 3;
      s.story3();
      player.chooseTrait();
      player.hp = player.maxHp;
      player.mp = player.maxMp;
    } else if (player.lvl >= 5 && act == 4) {
      ///Hat Yai
      act = 5;
      place = 4;
      s.story4();
      player.chooseTrait();
      player.hp = player.maxHp;
      player.mp = player.maxMp;
    }
  }

  void randomEncounter() {
    int encounter = random.nextInt(encounters.length);
    if (encounters[encounter].contains("Battle")) {
      randomBattle();
    } else if (encounters[encounter].contains("Rest")) {
      takeRest();
    } else {
      shop();
    }
  }

  void continueJourney() {
    checkAct();
    if (act != 5) {
      randomEncounter();
    } else {
      finalBoss();
    }
  }

  void characterInfo() {
    clearConsole();
    printHeading("Character Info");
    print(
        "${player.name}\tHp: ${player.hp}/${player.maxHp}\tMp:${player.mp}/${player.maxMp}");
    printSeperator(20);
    print("Lvl: ${player.lvl} \tXP: ${player.xp} \tGold: ${player.gold}");
    printSeperator(20);
    print(
        "Health Potion left : ${player.potHp}\tMagic Potion left : ${player.potMp}");
    printSeperator(20);

    if (player.numAtkUpgrades > 0) {
      print(
          "Offensive trait: ${player.atkUpgrades.elementAt(player.numAtkUpgrades - 1)}");
      printSeperator(20);
    }
    if (player.numDefUpgrades > 0) {
      print(
          "Defensive trait: ${player.defUpgrades.elementAt(player.numDefUpgrades - 1)}");
    }
    anythingToContinue();
  }

  void shop() {
    clearConsole();
    printHeading("You meet a merchant\n He offer you a item");
    bool randomSale = (Random().nextDouble() * 10 >= 5);
    int hpPrice = 100;
    int mpPrice = 100;
    if (randomSale) {
      print("Health Potion : ${hpPrice} gold.");
      printSeperator(20);
      print("Do you wanna buy one?\n(1) Yes\n(2) No");
      int input = readInt("->", 2);
      if (input == 1) {
        clearConsole();
        if (player.gold >= hpPrice) {
          printHeading("You have bought Potion for ${hpPrice} gold.");
          player.potHp++;
          player.gold -= hpPrice;
          anythingToContinue();
        } else {
          printHeading(
              "You don't have enough gold please come again next time");
          anythingToContinue();
        }
      } else {
        clearConsole();
        printHeading('"Please come again"');
        anythingToContinue();
      }
    } else {
      print("Magic Potion : ${mpPrice} gold.");
      printSeperator(20);
      print("Do you wanna buy one?\n(1) Yes\n(2) No");
      int input = readInt("->", 2);
      if (input == 1) {
        clearConsole();
        if (player.gold >= mpPrice) {
          printHeading("You have bought Potion for ${mpPrice} gold.");
          player.potMp++;
          player.gold -= mpPrice;
          anythingToContinue();
        } else {
          printHeading(
              "You don't have enough gold please come again next time");
          anythingToContinue();
        }
      } else {
        clearConsole();
        printHeading('"Please come again"');
        anythingToContinue();
      }
    }
  }

  void takeRest() {
    clearConsole();
    if (player.restLeft >= 1) {
      printHeading("Do you want to rest? (${player.restLeft} left)");
      print("(1) Yes\n(2) No");
      int input = readInt("->", 2);
      if (input == 1) {
        clearConsole();
        if (player.hp < player.maxHp || player.mp < player.maxMp) {
          player.hp = player.maxHp;
          player.mp = player.maxMp;
          print("You took the rest and restored all the pain");
          print("You hp is ${player.hp}/${player.maxHp} hp");
          print("You mp is ${player.mp}/${player.maxMp} mp");
          player.restLeft--;
          anythingToContinue();
        }
      } else {
        print("You don't need to rest");
        anythingToContinue();
      }
    }
  }

  void randomBattle() {
    clearConsole();
    printHeading("You encounter the enemy. Be ready to fight!");
    anythingToContinue();
    // battle(enemy(enemies[random.nextInt(encounters.length)], 50));
    if (act == 1) {
      int randomEnemy = Random().nextInt(2);
      if (randomEnemy == 0) {
        battle1(slime("Slime", 10));
      } else if (randomEnemy == 1) {
        battle2(goblin("Goblin", 10));
      }
    }
    if (act == 2) {
      int randomEnemy = Random().nextInt(3);
      if (randomEnemy == 0) {
        battle1(slime("Slime", 10));
      } else if (randomEnemy == 1) {
        battle2(goblin("Goblin", 10));
      } else if (randomEnemy == 2) {
        battle3(orc("Orc", 10));
      }
    }
    if (act == 3) {
      int randomEnemy = Random().nextInt(4);
      if (randomEnemy == 0) {
        battle1(slime("Slime", 10));
      } else if (randomEnemy == 1) {
        battle2(goblin("Goblin", 10));
      } else if (randomEnemy == 2) {
        battle3(orc("Orc", 10));
      } else if (randomEnemy == 3) {
        battle4(centaur("Centaur Warrunner", 10));
      }
    }
    if (act == 4) {
      int randomEnemy = Random().nextInt(5);
      if (randomEnemy == 0) {
        battle1(slime("Slime", 10));
      } else if (randomEnemy == 1) {
        battle2(goblin("Goblin", 10));
      } else if (randomEnemy == 2) {
        battle3(orc("Orc", 10));
      } else if (randomEnemy == 3) {
        battle4(centaur("Centaur Warrunner", 10));
      } else if (randomEnemy == 4) {
        battle5(golem("Golem", 10));
      }
    }
  }

  void battle1(slime Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack() - player.defend());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          printSeperator(15);
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          printSeperator(15);
          player.hp -= dmgTook;
        } else {
          printHeading("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp <= 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack() - player.defend());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          if (player.potHp > 0) {
            player.hp = player.maxHp;
            player.potHp--;
            clearConsole();
            printHeading(
                "You used Health potion. You HP get restore back to ${player.maxHp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        } else if (input == 2) {
          if (player.potMp > 0) {
            player.mp = player.maxMp;
            player.potMp--;
            clearConsole();
            printHeading(
                "You used Magic potion. You MP get restore back to ${player.maxMp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        }
      } else {
        clearConsole();
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void battle2(goblin Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack() - player.defend());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          printSeperator(15);
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          printSeperator(15);
          player.hp -= dmgTook;
        } else {
          printHeading("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp <= 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack() - player.defend());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          if (player.potHp > 0) {
            player.hp = player.maxHp;
            player.potHp--;
            clearConsole();
            printHeading(
                "You used Health potion. You HP get restore back to ${player.maxHp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        } else if (input == 2) {
          if (player.potMp > 0) {
            player.mp = player.maxMp;
            player.potMp--;
            clearConsole();
            printHeading(
                "You used Magic potion. You MP get restore back to ${player.maxMp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        }
      } else {
        clearConsole();
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void battle3(orc Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} heal $dmgTook hp");
          Enemy.hp += dmgTook;
          if (Enemy.hp > Enemy.maxHp) {
            Enemy.hp = Enemy.maxHp;
          }
        } else {
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp < 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} heal $dmgTook hp");
              Enemy.hp += dmgTook;
              Enemy.hp -= dmg1 + dmg2;
              if (Enemy.hp > Enemy.maxHp) {
                Enemy.hp = Enemy.maxHp;
              }
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} heal $dmgTook hp");
              Enemy.hp += dmgTook;
              if (Enemy.hp > Enemy.maxHp) {
                Enemy.hp = Enemy.maxHp;
              }
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} heal $dmgTook hp");
              Enemy.hp += dmgTook;
              if (Enemy.hp > Enemy.maxHp) {
                Enemy.hp = Enemy.maxHp;
              }
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} heal $dmgTook hp");
              Enemy.hp += dmgTook;
              if (Enemy.hp > Enemy.maxHp) {
                Enemy.hp = Enemy.maxHp;
              }
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          player.hp = player.maxHp;
          player.potHp--;
          clearConsole();
          printHeading(
              "You used Health potion. You HP get restore back to ${player.maxHp}");
          anythingToContinue();
          clearConsole();
          if (eneSkill) {
            int dmgTook = (Enemy.uniAttack());
            print("The ${Enemy.name} used ${Enemy.skill}!");
            print("The ${Enemy.name} heal $dmgTook hp");
            Enemy.hp += dmgTook;
            if (Enemy.hp > Enemy.maxHp) {
              Enemy.hp = Enemy.maxHp;
            }
          } else {
            int dmgTook = (Enemy.attack() - player.defend());
            print("The ${Enemy.name} dealt $dmgTook damage to you");
            player.hp -= dmgTook;
          }
          anythingToContinue();
        } else if (input == 2) {
          player.mp = player.maxMp;
          player.potMp--;
          clearConsole();
          printHeading(
              "You used Magic potion. You MP get restore back to ${player.maxMp}");
          anythingToContinue();
          clearConsole();
          if (eneSkill) {
            int dmgTook = (Enemy.uniAttack());
            print("The ${Enemy.name} used ${Enemy.skill}!");
            print("The ${Enemy.name} heal $dmgTook hp");
            Enemy.hp += dmgTook;
            if (Enemy.hp > Enemy.maxHp) {
              Enemy.hp = Enemy.maxHp;
            }
          } else {
            int dmgTook = (Enemy.attack() - player.defend());
            print("The ${Enemy.name} dealt $dmgTook damage to you");
            player.hp -= dmgTook;
          }
          anythingToContinue();
        }
      } else {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} heal $dmgTook hp");
              Enemy.hp += dmgTook;
              if (Enemy.hp > Enemy.maxHp) {
                Enemy.hp = Enemy.maxHp;
              }
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void battle4(centaur Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack() - player.defend());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          printSeperator(15);
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          printSeperator(15);
          player.hp -= dmgTook;
        } else {
          printHeading("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp <= 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack() - player.defend());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          if (player.potHp > 0) {
            player.hp = player.maxHp;
            player.potHp--;
            clearConsole();
            printHeading(
                "You used Health potion. You HP get restore back to ${player.maxHp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        } else if (input == 2) {
          if (player.potMp > 0) {
            player.mp = player.maxMp;
            player.potMp--;
            clearConsole();
            printHeading(
                "You used Magic potion. You MP get restore back to ${player.maxMp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        }
      } else {
        clearConsole();
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void battle5(golem Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack() - player.defend());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          printSeperator(15);
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          printSeperator(15);
          player.hp -= dmgTook;
        } else {
          printHeading("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp <= 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack() - player.defend());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          if (player.potHp > 0) {
            player.hp = player.maxHp;
            player.potHp--;
            clearConsole();
            printHeading(
                "You used Health potion. You HP get restore back to ${player.maxHp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        } else if (input == 2) {
          if (player.potMp > 0) {
            player.mp = player.maxMp;
            player.potMp--;
            clearConsole();
            printHeading(
                "You used Magic potion. You MP get restore back to ${player.maxMp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        }
      } else {
        clearConsole();
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void bossBattle(boss Enemy) {
    while (true) {
      clearConsole();
      printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
      printHeading("${player.name}\nHp: ${player.hp}"
          "/${player.maxHp}\tMp: ${player.mp}"
          "/${player.maxMp}");
      print("Choose an action:");
      printSeperator(20);
      print("(1) Attack\n(2) Skill\n(3) Use Potion\n(4) Run Away");
      int input = readInt("->", 4);
      if (input == 1) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        int dmg = (player.attack() - Enemy.defend());
        int dmgTook = (Enemy.attack() - player.defend());
        if (dmgTook < 0) {
          dmg -= (dmgTook ~/ 2);
          dmgTook = 0;
        }
        if (dmg < 0) {
          dmg = 0;
        }
        Enemy.hp -= dmg;
        clearConsole();
        printHeading("Battle");
        print("You dealt " "$dmg" " damage to the ${Enemy.name}.");
        printSeperator(15);
        // print("The ${Enemy.name} dealt $dmgTook damage to you");
        if (eneSkill) {
          int dmgTook = (Enemy.uniAttack() - player.defend());
          if (dmgTook < 0) {
            dmg -= (dmgTook ~/ 2);
            dmgTook = 0;
          }
          printSeperator(15);
          print("The ${Enemy.name} used ${Enemy.skill}!");
          print("The ${Enemy.name} dealt $dmgTook damage to you");
          printSeperator(15);
          player.hp -= dmgTook;
        } else {
          printHeading("The ${Enemy.name} dealt $dmgTook damage to you");
          player.hp -= dmgTook;
        }
        anythingToContinue();
        if (player.hp <= 0) {
          playerDied();
          break;
        } else if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      } else if (input == 2) {
        clearConsole();
        printHeading("${Enemy.name}\nHp: ${Enemy.hp}" "/${Enemy.maxHp}");
        printHeading("${player.name}\nHp: ${player.hp}" "/${player.maxHp}");
        print("Choose your skill:");
        printSeperator(20);
        print("(1) Double Hit\n(2) Heal\n(3) Break Armor\n(4) Suicide");
        int input = readInt("->", 4);
        if (input == 1) {
          if (player.mp >= 30) {
            bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
            player.mp -= 30;
            clearConsole();
            if (eneSkill) {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.uniAttack() - player.defend());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            } else {
              int dmg1 = (player.attack() - Enemy.defend());
              int dmg2 = (player.attack() - Enemy.defend());
              print("You dealt " "$dmg1" " damage to the ${Enemy.name}.");
              printSeperator(15);
              print("You dealt " "$dmg2" " damage to the ${Enemy.name}.");
              printSeperator(15);
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              printSeperator(15);
              player.hp -= dmgTook;
              Enemy.hp -= dmg1 + dmg2;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 4 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();

              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 2) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 20) {
            clearConsole();
            player.mp -= 20;
            int heal = 50;
            player.hp += heal;
            if (player.hp > player.maxHp) {
              player.hp = player.maxHp;
            }
            printHeading(
                "ํYou heal yourself for $heal hp. You are now ${player.hp}/${player.maxHp} HP");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            } else if (Enemy.hp <= 0) {
              clearConsole();
              printHeading("You defeated the ${Enemy.name} !");
              player.xp += Enemy.xp;
              print("You earned ${Enemy.xp} xp !");
              bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
              int goldEarn = (Enemy.xp - Random().nextInt(10));
              if (addRest) {
                player.restLeft++;
                print("You found a tent that can be rest to heal your wound");
              }
              if (goldEarn > 0) {
                player.gold += goldEarn;
                print("You collect ${goldEarn} gold from the ${Enemy.name}");
              }
              lvlUp();
              anythingToContinue();
              break;
            }
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 3) {
          bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
          if (player.mp >= 40 && skill3 == 0) {
            clearConsole();
            skill3 = 3;
            player.mp -= 40;
            printHeading(
                "You inflict burning to ${Enemy.name}. it will receive burning damage for 3 turn");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else if (skill3 > 0) {
            clearConsole();
            printHeading("${Enemy.name} is already burning. Nothing happening");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
          } else {
            clearConsole();
            printHeading("You don't have enough MP to use this skill");
            anythingToContinue();
          }
        } else if (input == 4) {
          clearConsole();
          printHeading("You decide to suicide. Their is no hope anyway");
          anythingToContinue();
          clearConsole();
          printHeading("You took 9999 dmg by trying to kill yourself");
          anythingToContinue();
          player.hp -= player.hp;
          if (player.hp <= 0) {
            playerDied();
            break;
          }
        }
      } else if (input == 3) {
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        clearConsole();
        printHeading("Which one you want to use");
        print(
            "(1) Health Potion (${player.potHp} left)\n(2) Magic Potion (${player.potMp} left)");
        input = readInt("->", 2);
        if (input == 1) {
          if (player.potHp > 0) {
            player.hp = player.maxHp;
            player.potHp--;
            clearConsole();
            printHeading(
                "You used Health potion. You HP get restore back to ${player.maxHp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        } else if (input == 2) {
          if (player.potMp > 0) {
            player.mp = player.maxMp;
            player.potMp--;
            clearConsole();
            printHeading(
                "You used Magic potion. You MP get restore back to ${player.maxMp}");
            anythingToContinue();
            clearConsole();
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
          } else {
            clearConsole();
            printHeading("You don't have anymore too use");
          }
          anythingToContinue();
        }
      } else {
        clearConsole();
        bool eneSkill = (Random().nextDouble() * 5 + 1 <= 2.25);
        if (act != 4) {
          if (Random().nextDouble() * 10 + 1 <= 3.5) {
            printHeading("You ran away!");
            anythingToContinue();
            break;
          } else {
            printHeading("You didn't manage to escape");
            if (eneSkill) {
              int dmgTook = (Enemy.uniAttack());
              print("The ${Enemy.name} used ${Enemy.skill}!");
              print("The ${Enemy.name} dealt $dmgTook hp");
              Enemy.hp -= dmgTook;
            } else {
              int dmgTook = (Enemy.attack() - player.defend());
              print("The ${Enemy.name} dealt $dmgTook damage to you");
              player.hp -= dmgTook;
            }
            anythingToContinue();
            if (player.hp <= 0) {
              playerDied();
              break;
            }
          }
        } else {
          printHeading("You can't escape boss fight");
          anythingToContinue();
        }
      }
      if (skill3 > 0) {
        clearConsole();
        int burnDmg = 10 + ((Enemy.hp ~/ 100) * 5);
        printHeading(
            "${Enemy.name} Hp decrease by $burnDmg cause of burning effect");
        Enemy.hp -= burnDmg;
        skill3--;
        anythingToContinue();
        if (Enemy.hp <= 0) {
          clearConsole();
          printHeading("You defeated the ${Enemy.name} !");
          player.xp += Enemy.xp;
          print("You earned ${Enemy.xp} xp !");
          bool addRest = (Random().nextDouble() * 5 + 1 <= 2.25);
          int goldEarn = (Enemy.xp - Random().nextInt(10));
          if (addRest) {
            player.restLeft++;
            print("You found a tent that can be rest to heal your wound");
          }
          if (goldEarn > 0) {
            player.gold += goldEarn;
            print("You collect ${goldEarn} gold from the ${Enemy.name}");
          }
          lvlUp();
          anythingToContinue();
          break;
        }
      }
    }
  }

  void printMenu() {
    clearConsole();
    printHeading(places.elementAt(place));
    print("Choose an action:");
    printSeperator(20);
    print("(1) Continue on your journey");
    print("(2) Character Info");
    print("(3) Rest");
    print("(4) Exit Game");
  }

  void finalBoss() {
    clearConsole();
    printHeading(
        "After a long journey, finally you found the Big boss\nHe is the one who commit the war crime. You have to kill him..");
    anythingToContinue();
    bossBattle(boss("The Emungus", 1000));
    s.story4();
    isRunning = false;
  }

  void playerDied() {
    clearConsole();
    printHeading("You died");
    printHeading("You earned ${player.lvl} lvl on this entire journey");
    print("Goodbye");
    isRunning = false;
  }

  void gameLoop() {
    while (isRunning) {
      printMenu();
      int input = readInt("-> ", 4);
      if (input == 1) {
        continueJourney();
      } else if (input == 2) {
        characterInfo();
      } else if (input == 3) {
        takeRest();
      } else {
        isRunning = false;
      }
    }
  }
}
