import 'character.dart';
import 'dart:math';

class slime extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  slime(String name, int playerXp) : super(name, 50, 50, 10) {
    this.playerXp = playerXp;
    this.skill = "Sticky Tackle";
  }

  @override
  int attack() {
    return 5 + Random().nextInt(5);
  }

  int uniAttack() {
    return attack() + 10;
  }

  @override
  int defend() {
    return 1;
  }
}
