import 'character.dart';
import 'dart:math';

class goblin extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  goblin(String name, int playerXp) : super(name, 100, 50, 30) {
    this.playerXp = playerXp;
    this.skill = "Slash";
  }

  @override
  int attack() {
    return 15 + Random().nextInt(5);
  }

  int uniAttack() {
    return attack() + 20;
  }

  @override
  int defend() {
    return 5;
  }
}
