import 'character.dart';
import 'dart:math';

class enemy extends character {
  final random = Random();
  late int playerXp;

  // enemy(String name, int playerXp)
  //     : super(name, Random().nextDouble() * playerXp + playerXp + 5 / 3,
  //           Random().nextDouble() * playerXp / 4 + 3)
  enemy(String name, int playerXp) : super(name, 100, 50, 100) {
    this.playerXp = playerXp;
  }

  @override
  int attack() {
    return 10 + Random().nextInt(10);
  }

  @override
  int defend() {
    return 10;
  }
}
