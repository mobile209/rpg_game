import 'character.dart';
import 'dart:math';

class golem extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  golem(String name, int playerXp) : super(name, 500, 50, 100) {
    this.playerXp = playerXp;
    this.skill = "Rock Smash";
  }

  @override
  int attack() {
    return 50 + Random().nextInt(5);
  }

  int uniAttack() {
    return attack() + 50;
  }

  @override
  int defend() {
    return 35;
  }
}
