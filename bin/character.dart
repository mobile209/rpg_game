abstract class character {
  String name = "";
  late int maxHp = 0, hp = 0, xp = 0, mp, maxMp = 0;

  character(name, maxHp, maxMp, exp) {
    this.name = name;
    this.maxHp = maxHp;
    this.xp = exp;
    this.hp = maxHp;
    this.maxMp = maxMp;
    this.mp = maxMp;
  }

  void attack() {}

  void defend() {}
  String? get getName {
    return name;
  }

  set setName(String newName) {
    name = newName;
  }

  int get getMaxHp {
    return maxHp;
  }

  set setMaxHp(int newMaxHp) {
    maxHp = newMaxHp;
  }

  int get getHp {
    return hp;
  }

  set setHp(int newHp) {
    hp = newHp;
  }

  int get getXp {
    return xp;
  }

  set setXp(int newXp) {
    xp = newXp;
  }

  void info() {
    print("name: $name maxHp: $maxHp hp: $hp  xp: $xp");
  }
}
