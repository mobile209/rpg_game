import 'character.dart';
import 'dart:math';

class centaur extends character {
  final random = Random();
  late int playerXp;
  late String skill;

  centaur(String name, int playerXp) : super(name, 250, 50, 70) {
    this.playerXp = playerXp;
    this.skill = "Earth Stomp";
  }

  @override
  int attack() {
    return 35 + Random().nextInt(5);
  }

  int uniAttack() {
    return attack() + 30;
  }

  @override
  int defend() {
    return 20;
  }
}
