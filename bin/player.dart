import 'dart:math';

import 'character.dart';
import 'game.dart';

class Player extends character {
  game g = game();
  late int numAtkUpgrades, numDefUpgrades;
  late int gold, restLeft, potHp, potMp, lvl, atk, def;
  Set<String> atkUpgrades = {
    "Handgun",
    "Bomb",
    "Rail Gun",
    "Grenade Launcher",
    "Nuclear Missile"
  };
  Set<String> defUpgrades = {
    "Headlamps",
    "Motorcycle Helmet",
    "BulletProof Vest",
    "Bomb Suit",
    "Spaceship"
  };
  Player(String name) : super(name, 100, 50, 0) {
    this.numAtkUpgrades = 0;
    this.numDefUpgrades = 0;

    this.gold = 0;
    this.restLeft = 1;
    this.potHp = 1;
    this.potMp = 1;
    this.lvl = 1;
    this.atk = 0;
    this.def = 0;

    chooseTrait();
  }

  @override
  int attack() {
    return (lvl * 20) + Random().nextInt(10) + 10 + atk;
  }

  @override
  int defend() {
    return (lvl * 10 ~/ 5) + def;
  }

  void chooseTrait() {
    g.clearConsole();
    g.printHeading("Choose a trait:");
    print("(1) ${atkUpgrades.elementAt(numAtkUpgrades)}");
    print("(2) ${defUpgrades.elementAt(numDefUpgrades)}");

    int input = g.readInt("->", 2);
    g.clearConsole();
    if (input == 1) {
      g.printHeading("You choose ${atkUpgrades.elementAt(numAtkUpgrades)}!");
      numAtkUpgrades++;
      switch (numAtkUpgrades) {
        case 1:
          {
            atk += 5;
          }
          break;
        case 2:
          {
            atk += 10;
          }
          break;
        case 3:
          {
            atk += 15;
          }
          break;
        case 4:
          {
            atk += 20;
          }
          break;
        case 5:
          {
            atk += 25;
          }
          break;
      }
    } else {
      g.printHeading("You choose ${defUpgrades.elementAt(numDefUpgrades)}!");
      numDefUpgrades++;
      switch (numAtkUpgrades) {
        case 1:
          {
            def += 5;
          }
          break;
        case 2:
          {
            def += 10;
          }
          break;
        case 3:
          {
            def += 15;
          }
          break;
        case 4:
          {
            def += 20;
          }
          break;
        case 5:
          {
            def += 25;
          }
          break;
      }
    }
    g.anythingToContinue();
  }
}
